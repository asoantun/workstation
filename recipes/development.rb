%w(vim ruby git ).each do |p|
  package p do
    action :install
  end
end
