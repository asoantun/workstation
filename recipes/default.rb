#
# Cookbook Name:: workstation
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#

if node[:platform].include?('debian')
  %w(iceweasel icedove).each do |p|
    package p do
      action :install
    end
  end
else
  package %(firefox thunderbird).each do |p|
    action :install
  end
end

%w(chromium pidgin gajim kate konsole shutter).each do |p|
  package p do
    action :install
  end
end
